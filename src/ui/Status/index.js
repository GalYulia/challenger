import React from 'react';
import './styles.css';

const Status = (active) => (
    <div className="status">
        {active ?
            <div className="status__wrapper">
                <img src={require('./active.svg')} alt="Active" width="20" height="20"/>
                Активен
            </div> :
            <div className="status__wrapper">
                <img src={require('./disable.svg')}  alt="Not active" width="20" height="20"/>
                Неактивен
            </div>
        }
    </div>
);

export default Status;

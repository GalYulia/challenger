import React from 'react';
import './styles.css';

const Button = ({ href, onClick, text, disabled }) => {
    const className = disabled ? 'button button_disabled' : 'button';

    return href ?
        <a href={href} className={className}>{text}</a> :
        <div className={className}
             onClick={() => onClick && onClick()}>{text}</div>;
};

export default Button;

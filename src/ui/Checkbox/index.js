import React, { Component } from 'react';
import './styles.css';

export default class Checkbox extends Component {
    state = {
        value: this.props.initialValue,
    };

    handleChange = (e) => {
        const { onChange } = this.props;
        if (onChange) onChange(e);

        this.setState(prevState => ({
            value: !prevState.value
        }));
    };

    render() {
        const { name, disabled } = this.props;
        const { value } = this.state;
        let style = 'checkbox';
        if (value) style = style + ' checkbox--checked';
        if (disabled) style = style + ' checkbox--disabled';
        return (
            <div className="check-wrapper">
                <div className={style} onClick={!disabled && this.handleChange}/>
                <span className="checkbox__value">{name}</span>
            </div>
        );
    }
};

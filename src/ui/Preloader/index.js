import React from 'react';
import './styles.css';
import icon from './preloader.svg';

const Preloader = () => (
    <div className="preloader">
        <div className="preloader__container">
            <img src={icon} alt="Loading..." className="preloader__icon"/>
        </div>
    </div>
);

export default Preloader

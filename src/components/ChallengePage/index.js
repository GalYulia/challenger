import React, {Component} from 'react';
import {connect} from "react-redux";
import cookies from 'react-cookies';
import axios from 'axios';
import {baseUrl} from "../../api";
import Checkbox from '../../ui/Checkbox';
import Preloader from '../../ui/Preloader';
import Button from '../../ui/Button';
import Status from "../../ui/Status";
import {startChallenge} from "../../store/reducers/challenge";
import './styles.css';
import {ACCESS_TOKEN} from "../../store/actions/auth";

class ChallengePage extends Component {
    state = {
        isStartOptionsHidden: true
    };

    handlePreStartClick = () => {
        this.setState({
            isStartOptionsHidden: false
        });
    };

    handleStartClick = () => {
        const id = this.props.challenge.id;
        this.props.startChallenge(id);

        window.location= "/";

    };

    handleNext = (idPoint, idCh) => {
        axios.post(`${baseUrl}/point/${idPoint}/next?open_challenge_id=${idCh}`, {
            },
            {
                headers: {'Authorization': 'Bearer '+ cookies.load(ACCESS_TOKEN)}
            });
    };

    renderStart = () => {
        const { own_position_ord } = this.props;
        const { isStartOptionsHidden } = this.state;

        if (!own_position_ord && isStartOptionsHidden)
            return <div className="button-container">
                <Button text="Начать" onClick={this.handleStartClick}/>
            </div>;

        // if (!own_position_ord && !isStartOptionsHidden)
        //     return <div className="button-container">
        //         <span className="question">С кем вы хотите пройти этот путь?</span>
        //         <div className="row">
        //             <Button text="Один" onClick={this.handleStartClick}/>
        //             <Button text="С другом" onClick={this.handleStartClick}/>
        //         </div>
        //     </div>;
    };

    render() {
        const {challenge, plan, own_position_ord, date_end, partner_name, partner_position_ord } = this.props;
        if (!challenge) return null;

        console.log('1234',!!own_position_ord)
        return (
            <div className="plan">
                <div className="plan__header">
                    <h2>{challenge.name}</h2>
                    <Status active={!!own_position_ord}/>
                </div>
                {plan.length ? plan.map((stage, id) => {
                    const name = stage.name || '';
                    let date = '';
                    if (id === plan.length - 1 && date_end) date = ' до ' + new Date(date_end).toLocaleString();
                    if (id !== plan.length - 1 && plan[id+1].date_start)
                        date = ' до ' + new Date(plan[id+1].date_start).toLocaleString();
                    return (<div className="stage">
                        <Checkbox initialValue={stage.id <= own_position_ord}
                                  name={`${name}${date}`}
                                  disabled={plan[(id || 1)  - 1].id !== own_position_ord}
                                  onChange={() => this.handleNext(stage.id, challenge.id)}
                        />
                        {id === own_position_ord - 1 && <span className="name me">@me</span>}
                        {id === partner_position_ord - 1 && <span className="name partner">{`@${partner_name}`}</span>}
                    </div>
            )}) : <Preloader/>}
            {this.renderStart()}
        </div>
        );
    }
};

const mapDispatchToProps = {
    startChallenge
};

export default connect(null, mapDispatchToProps)(ChallengePage);

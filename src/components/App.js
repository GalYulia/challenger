import React from "react";
import { Provider } from 'react-redux';
import { ConnectedRouter as Router } from 'connected-react-router';
import { Route, Switch } from 'react-router';
import history from '../history';
import { configureStore } from '../store/configureStore';
import ItemList from '../containers/ItemsList';
import Layout from './Layout';
import AccountPage from '../containers/AccountPage';
import ChallengePageContainer from '../containers/ChallengePage';
import '../styles/reset.css';

const store = configureStore();


const App = () => {
    return(
        <Provider store={store}>
            <Router history={history}>
                <Layout>
                    <Switch>
                        <Route exact path="/" component={ItemList}/>
                        <Route path="/account-page" component={AccountPage} />
                        <Route path="/challenge/:id"
                               component={ChallengePageContainer}
                        />
                    </Switch>
                </Layout>
            </Router>
        </Provider>
    )
};

export default (App)

import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './styles.css';

class Card extends Component  {
    render() {
        const {name, icon_link, description, id} = this.props;
        return (
            <a href={`/challenge/${id}`} >
                <div className='card-container'>
                    <div className='card-container__image'><img src={icon_link}/></div>
                    <div className='card-container__name'><h1>{name}</h1></div>
                    <div className='card-container__description'>{description}</div>
                </div>
            </a>
        )
    }
}

export default Card

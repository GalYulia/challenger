import React, { Component } from "react";
import { connect } from "react-redux";
import Button from "../../ui/Button";
import './styles.css';
import { check } from "../../store/actions/auth";
import { acceptChallenge, declineChallenge } from "../../store/reducers/lk";

class Notification extends Component  {

    onClick = () => {
        this.props.acceptChallenge(this.props.date.open_challenge_id);
    };

    onClickDelete = () => {
        this.props.declineChallenge(this.props.date.open_challenge_id);
    };

    render() {
        const { notification_type, information } = this.props.date;
        return (
            <div className={`notification-container notification-container--${notification_type}`}>
                <div className={`notification-container__block`}>
                    <div>{information}</div>
                    <div>{notification_type}</div>
                </div>
                {notification_type==='INVITE' &&
                <div className="notification-container__block">
                    <Button text="Принять" onClick={this.onClick}/>
                    <Button text="Отменить" onClick={this.onClickDelete}/>
                </div>}
            </div>

        )
    }
}

const mapDispatchToProps = {
    acceptChallenge,
    declineChallenge
};

export default connect(null, mapDispatchToProps)(Notification);

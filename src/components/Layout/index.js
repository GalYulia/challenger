import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from '../../ui/Button';
import { baseUrl } from '../../api';
import { logout } from '../../store/actions/auth';
import './styles.css';

class Layout extends Component {
    render() {
        return (
            <div>
                <header className='header'>
                    <div className='navigation'>
                        <a href="/">Главная</a>
                        <a href="/account-page">Личный кабинет</a>
                        <Button
                            href={!this.props.token && `${baseUrl}/auth/vk/login`}
                            onClick={this.props.token ? this.props.logout : null}
                            text={this.props.token ? 'Log Out' : 'Log In'}
                        />
                    </div>
                </header>
                <main className='container'>{this.props.children}</main>
                <footer className='footer'>А вот сын маминой подруги уже миллион заработал</footer>
            </div>
        );
    }
};

export default connect(
    state => ({ token: state.auth.token }),
    { logout },
)(Layout);
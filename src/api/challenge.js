import { baseUrl } from './index';
import axios from 'axios';

export function getChallenge (id) {
  return axios.get(`${baseUrl}/challenge/${id}`);
}
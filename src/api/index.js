import axios from 'axios';

export const baseUrl = 'http://192.168.43.156:8000';

const challengerRequest = axios.create({
    baseURL: baseUrl,
});

export default challengerRequest;

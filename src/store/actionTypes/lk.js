export const USER_INFO_FETCHED = 'USER_INFO_FETCHED';
export const USER_INFO_SUCCEEDED = 'USER_INFO_SUCCEEDED';
export const USER_INFO_FAILED = 'USER_INFO_FAILED';

export const ACCEPT_CHALLENGE = 'ACCEPT_CHALLENGE';
export const ACCEPT_CHALLENGE_FAILED='ACCEPT_CHALLENGE_FAILED';

export const DELETE_CHALLENGE = 'DELETE_CHALLENGE';
export const DELETE_CHALLENGE_FAILED='DELETE_CHALLENGE_FAILED'
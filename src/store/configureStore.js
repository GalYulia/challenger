import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import { routerMiddleware } from "connected-react-router";
import history from '../history';

export const configureStore = () => {
    const middleware = [
        thunk,
        routerMiddleware(history),
    ];

    const enhancer = compose(
        applyMiddleware(...middleware),
    );

    return createStore(
        rootReducer(history),
        {},
        enhancer,
    );
};
import cookies from 'react-cookies';
import {SET_LOGIN} from '../actionTypes/auth';

export const ACCESS_TOKEN = 'ACCESS_TOKEN';

export function check () {
    return dispatch => {
        const token = cookies.load(ACCESS_TOKEN);
        if (token === 'null') cookies.save(ACCESS_TOKEN, null);
        return dispatch({
            type: SET_LOGIN,
            token,
        })
    }
}

export function login (token) {
    return dispatch => {
        cookies.save(ACCESS_TOKEN, token);
        return dispatch({
            type: SET_LOGIN,
            token,
        })
    }
}

export function logout() {
    return dispatch => {
        cookies.remove(ACCESS_TOKEN);
        return dispatch({
            type: SET_LOGIN,
            token: null,
        })
    }
}

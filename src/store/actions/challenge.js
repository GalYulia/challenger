import {
  CHALLENGE_REQUEST,
  CHALLENGE_SUCCESS,
  CHALLENGE_ERROR,
} from '../actionTypes/challenge';

//action creators
export const challengeRequest = () => ({
  type: 'CHALLENGE_REQUEST'
});

export const challengeError = () => ({
  type: 'CHALLENGE_ERROR'
});

export const challengeSuccess = payload => ({
  type: 'CHALLENGE_SUCCESS',
  payload
});

export const succeededChallengeStart = () => ({
  type: 'START_CHALLENGE'
});

export const failedChallengeStart = () => ({
  type: 'FAILED_CHALLENGE_START'
});
//action creators
import {
    ACCEPT_CHALLENGE,
    ACCEPT_CHALLENGE_FAILED, DELETE_CHALLENGE, DELETE_CHALLENGE_FAILED,
    USER_INFO_FAILED,
    USER_INFO_FETCHED,
    USER_INFO_SUCCEEDED
} from "../actionTypes/lk";

export const userInfoFetched = () => ({
    type: USER_INFO_FETCHED
});

export const userInfoFailed = () => ({
    type: USER_INFO_FAILED
});

export const userInfoSucceded = payload => ({
    type: USER_INFO_SUCCEEDED,
    payload
});

export const acceptChallengeSucceded = () => ({
    type: ACCEPT_CHALLENGE,
});

export const acceptChallengeFailed = () => ({
    type: ACCEPT_CHALLENGE_FAILED,
});


export const deleteChallengeSucceded = () => ({
    type: DELETE_CHALLENGE,
});

export const deleteChallengeFailed = () => ({
    type: DELETE_CHALLENGE_FAILED,
});




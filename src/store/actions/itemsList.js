import {
    CHALLENGE_LIST_FETCHED,
    CHALLENGE_LIST_FAILED,
    CHALLENGE_LIST_SUCCEEDED,
} from '../actionTypes/itemsList';

//action creators
export const challengeListFetched = () => ({
    type: 'CHALLENGE_LIST_FETCHED'
});

export const challengeListFailed = () => ({
    type: 'CHALLENGE_LIST_FAILED'
});

export const challengeListSucceded = payload => ({
    type: 'CHALLENGE_LIST_SUCCEEDED',
    payload
});
import axios from 'axios';
import {
    challengeListFailed,
    challengeListFetched,
    challengeListSucceded,
} from "../actions/itemsList";
import { baseUrl } from "../../api";

//middlware
export const fetchItemsList = () => (dispatch, getState) => {
    dispatch(challengeListFetched());

    return axios
        .get(`${baseUrl}/challenge`, {
            params: {
                offset: 0,
                limit: 10
            },
            headers: {'Authorization': 'Bearer '+ getState().auth.token}
        })
        .then(response => {
            return dispatch( challengeListSucceded({ isLoading: false, items: response.data }));
        })
        .catch(() => dispatch(challengeListFailed(false)));
};


const initialState = {
    items: [],
    isLoading: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'CHALLENGE_LIST_FETCHED':
            return {...state, isLoading: true};
        case 'CHALLENGE_LIST_FAILED':
            return {...state, isLoading: false};
        case 'CHALLENGE_LIST_SUCCEEDED':
            return {...state, isLoading: false, items: action.payload.items};
        default: return state;
    }
}
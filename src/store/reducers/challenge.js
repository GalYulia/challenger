import {
    CHALLENGE_REQUEST,
    CHALLENGE_SUCCESS,
    CHALLENGE_ERROR
} from '../actionTypes/challenge';
import axios from "axios";
import cookies from 'react-cookies';
import {
    challengeError,
    challengeRequest,
    challengeSuccess,
    failedChallengeStart,
    succeededChallengeStart
} from "../actions/challenge";
import {baseUrl} from "../../api";
import {ACCESS_TOKEN} from "../actions/auth";

const initialState = {
    plan: [],
    name: '',
    loading: false,
    myPosition: -1,
};

//middlware
export const fetchChallengeInfo = id => (dispatch, getState) => {
    dispatch(challengeRequest());

    return axios
        .get(`${baseUrl}/challenge/${id}`, {
            headers: {'Authorization': 'Bearer '+ getState().auth.token || cookies.load(ACCESS_TOKEN)}
        })
        .then(response => {
            dispatch(challengeSuccess({ isLoading: false, plan: response.data }));
        })
        .catch(() => dispatch(challengeError(false)));
};

export const startChallenge = (challengeId) => (dispatch, getState) => {
    return axios
        .get(`${baseUrl}/challenge/${challengeId}/start`, {
            headers: {'Authorization': 'Bearer '+ getState().auth.token || cookies.load(ACCESS_TOKEN)}
        })
        .then(() => {
            dispatch( succeededChallengeStart());
            dispatch(fetchChallengeInfo(id));
        })
        .catch(() => dispatch(failedChallengeStart()));
};

export default function (state = initialState, action) {
    switch (action.type) {
        case CHALLENGE_REQUEST: return ({ loading: true });
        case CHALLENGE_SUCCESS: return ({
            loading: false,
            ...action.payload,
        });
        case CHALLENGE_ERROR: return ({ loading: false });

        default: return state;
    }
};

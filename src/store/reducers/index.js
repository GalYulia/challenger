import { combineReducers } from 'redux';
import auth from './auth';
import itemsList from './itemsList';
import challenge from './challenge';
import lk from './lk';
import { connectRouter } from 'connected-react-router';

export default history => combineReducers({
    router: connectRouter(history),
    itemsList,
    challenge,
    auth,
    lk
})

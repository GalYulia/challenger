import axios from 'axios';
import {baseUrl} from "../../api";
import {
    acceptChallengeFailed,
    acceptChallengeSucceded, deleteChallengeFailed, deleteChallengeSucceded,
    userInfoFailed,
    userInfoFetched,
    userInfoSucceded
} from "../actions/lk";

//middlware
export const fetchUserInfo = () => (dispatch, getState) => {
    dispatch(userInfoFetched());

    return axios
        .get(`${baseUrl}/user/account`, {
                headers: {'Authorization': 'Bearer '+ getState().auth.token}
            }
        )
        .then(response => {
            return dispatch( userInfoSucceded({ isLoading: false, items: response.data }));
        })
        .catch(() => dispatch(userInfoFailed()));
};


const initialState = {
    items: [],
    isLoading: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'USER_INFO_FETCHED':
            return {...state, isLoading: true};
        case 'USER_INFO_FAILED':
            return {...state, isLoading: false};
        case 'USER_INFO_SUCCEEDED':
            return {...state, isLoading: false, items: action.payload.items};
        default: return state;
    }
}


export const acceptChallenge = openChallengeId => (dispatch, getState) => {
    return axios
        .get(`${baseUrl}/challenge/${openChallengeId}/accept`, {
                headers: {'Authorization': 'Bearer '+ getState().auth.token}
            }
        )
        .then(() => {
            return dispatch( acceptChallengeSucceded());
        })
        .catch(error => {
            dispatch(acceptChallengeFailed(error))
            alert(error.message)
        });
};


export const declineChallenge = openChallengeId => (dispatch, getState) => {
    return axios
        .delete(`${baseUrl}/challenge/${openChallengeId}/end`, {
                headers: {'Authorization': 'Bearer '+ getState().auth.token}
            }
        )
        .then(() => {
            return dispatch( deleteChallengeSucceded());
        })
        .catch(error => {
            dispatch(deleteChallengeFailed(error))
            alert(error.message)
        });
};


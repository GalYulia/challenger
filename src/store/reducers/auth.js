import { SET_LOGIN } from '../actionTypes/auth';

const initialState = {
  token: null
};

export default function (state = initialState, action) {
  switch (action.type) {
      case SET_LOGIN: return ({
          ...state,
          token: action.token,
      });

      default: return state;
  }
}

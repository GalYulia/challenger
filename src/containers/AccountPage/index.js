import React, { Component } from "react";
import { connect } from 'react-redux';
import ItemsList from "../ItemsList";
import { check } from "../../store/actions/auth";
import { fetchUserInfo } from "../../store/reducers/lk";
import Notification from "../../components/Notification";
import "./styles.css";

class AccountPage extends Component{
    componentDidMount () {
        this.props.check();
        this.props.fetchUserInfo();
    }

    render() {
        const {own_notifications} = this.props.items;

        return (
            <div>
                <h1>Твои челленжи</h1>
                <ItemsList type="own"/>
                <h1>Уведомления</h1>
                <div className="account-container">
                    {own_notifications && own_notifications.map((date) => (
                        <Notification date={date} />
                        ))}
                </div>
            </div>
        );
    }
};

const mapStateToProps = (state) => {
    return ({
        token: state.auth.token,
        items: state.lk.items,
        isLoading: state.lk.isLoading
    });
};

const mapDispatchToProps = {
    check,
    fetchUserInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);

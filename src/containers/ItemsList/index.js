import React, { Component } from 'react';
import { connect } from 'react-redux';
import Card from '../../components/Card';
import Preloader from '../../ui/Preloader';
import { login, check } from '../../store/actions/auth';
import { fetchItemsList } from '../../store/reducers/itemsList';
import './styles.css';

class ItemsList extends Component {
    componentDidMount() {
        const urlParams = new URLSearchParams(window.location.search);
        const token =  urlParams.get('token');

        if (token) {
            this.props.login(token);
        } else {
            this.props.check();
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.token !== prevProps.token) {
            this.props.fetchItemsList();
        }
    }

    render() {
        if (this.props.isLoading) {
            return <Preloader/>;
        }

        const items = this.props.items && (this.props.type === "own" ? this.props.items['own'] : this.props.items['all']);

        return (<div className='itemlist-container'>
                {items && items.map((item) => (
                        <Card {...item}/>
                ))}
            </div>
        );
    }
}

ItemsList.defaultProps = {
    type: "all"
};

const mapStateToProps = (state) => {
    return ({
        items: state.itemsList.items,
        isLoading: state.itemsList.isLoading,
        token: state.auth.token,
    });
};

const mapDispatchToProps = {
    fetchItemsList,
    login,
    check,
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
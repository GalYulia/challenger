import React, {Component} from 'react';
import {connect} from 'react-redux';
import ChallengePage from '../../components/ChallengePage';
import Preloader from '../../ui/Preloader';
import {check} from '../../store/actions/auth';
import {fetchChallengeInfo as getInfo} from '../../store/reducers/challenge';

class ChallengePageContainer extends Component {
  componentDidMount() {
      this.props.check();
      const id = this.props.match.params.id || 1;
      this.props.getInfo(id);
  }

  render() {
      const plan = this.props.plan.plan  && this.props.plan.plan.control_points || [];
      const otherProps = this.props.plan.plan;

      if (!plan.length) return <Preloader/>;

      return (<ChallengePage
          plan={plan}
          challenge={otherProps && otherProps.challenge}
          own_position_ord={otherProps && otherProps.own_position_ord}
          date_end={otherProps && otherProps.date_end}
          partner_name={otherProps && otherProps.partner_name}
          partner_position_ord={otherProps && otherProps.partner_position_ord}
      />);
  }
}
const mapStateToProps = (state) => {
  return ({
    token: state.auth.token,
    plan: state.challenge,
    isLoading: state.challenge.isLoading
  });
};

const mapDispatchToProps = {
    check,
    getInfo
};

export default connect(mapStateToProps, mapDispatchToProps)(ChallengePageContainer);

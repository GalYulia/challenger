module.exports = {
  plugins: {
    'postcss-preset-env':{  browsers: 'last 2 versions'},
    'postcss-font-magician': {
      variants: {
        'Segoe UI Light': {
          '300': [],
          '400': [],
          '700': []
        }
      },
      foundries: ['google']
    },
    'postcss-nested': {},
    'postcss-import':{}
  }
};
